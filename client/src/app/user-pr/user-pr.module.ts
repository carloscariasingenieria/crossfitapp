import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UserPrPageRoutingModule } from './user-pr-routing.module';

import { UserPrPage } from './user-pr.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UserPrPageRoutingModule
  ],
  declarations: [UserPrPage]
})
export class UserPrPageModule {}
