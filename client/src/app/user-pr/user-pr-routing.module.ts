import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UserPrPage } from './user-pr.page';

const routes: Routes = [
  {
    path: '',
    component: UserPrPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserPrPageRoutingModule {}
