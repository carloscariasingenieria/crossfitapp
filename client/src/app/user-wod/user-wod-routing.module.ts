import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UserWodPage } from './user-wod.page';

const routes: Routes = [
  {
    path: '',
    component: UserWodPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserWodPageRoutingModule {}
