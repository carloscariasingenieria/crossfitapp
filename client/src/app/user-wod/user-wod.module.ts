import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UserWodPageRoutingModule } from './user-wod-routing.module';

import { UserWodPage } from './user-wod.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UserWodPageRoutingModule
  ],
  declarations: [UserWodPage]
})
export class UserWodPageModule {}
