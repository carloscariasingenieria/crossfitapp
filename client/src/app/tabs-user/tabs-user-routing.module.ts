import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabsUserPage } from './tabs-user.page';

const routes: Routes = [
  {
    path: 'usertabs',
    component: TabsUserPage,
    children: [
      {
        path: 'prmax',
        loadChildren: () => import('../user-pr/user-pr.module').then(m => m.UserPrPageModule)
      },
      {
        path: 'wod',
        loadChildren: () => import('../user-wod/user-wod.module').then(m => m.UserWodPageModule)
      },
      {
        path: 'perfil',
        loadChildren: () => import('../user-perfil/user-perfil.module').then(m => m.UserPerfilPageModule)
      },
      {
        path: '',
        redirectTo: '/usertabs/wod',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/usertabs/wod',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsUserPageRoutingModule {}
